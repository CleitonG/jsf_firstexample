/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.mack.controller;

import br.mack.entity.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author joaquim
 */
@ManagedBean
public class UserController {
    
    private List<Usuario> usuarios = new ArrayList<>();
    private Usuario novoUsuario = new Usuario();
    
    
    public List<Usuario> getUsuarios() {
        return this.usuarios;
    }

    /**
     * @param usuarios the usuarios to set
     */
    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * @return the novoUsuario
     */
    public Usuario getNovoUsuario() {
        return novoUsuario;
    }

    /**
     * @param novoUsuario the novoUsuario to set
     */
    public void setNovoUsuario(Usuario novoUsuario) {
        this.novoUsuario = novoUsuario;
    }
    
    public void adicionar(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        
        for (Usuario usuario : usuarios) {
            if(usuario.getNome().equals(novoUsuario.getNome())){
                facesContext.addMessage(null, new FacesMessage("Já existe um usuário com este nome"));
                return;
            }
        }
        
        usuarios.add(novoUsuario);
        novoUsuario = new Usuario();
        
        facesContext.addMessage(null, new FacesMessage("Usuario inserido"));
    }
    
    public void remover(String nome){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        
        if (nome==null || nome.trim().equals("")) {
            facesContext.addMessage(null, new FacesMessage("Informe o nome do usuário para remover"));
            return;
        }
        
        for (Usuario usuario : usuarios) {
            if(usuario.getNome().equals(nome)){
                usuarios.remove(usuario);
                facesContext.addMessage(null, new FacesMessage("Usuário " + usuario.getNome() + " removido!"));
                return;
            }
        }
        facesContext.addMessage(null, new FacesMessage("Usuário não encontrado"));
    }
}
